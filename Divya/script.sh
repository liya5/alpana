#!/bin/bash
echo "[alpana_info]: Downloading and installing required packages."
pacman -S vlc lightdm-slick-greeter lightdm-settings terminus-font --noconfirm --needed
echo "[alpana_info]: Uninstalling unneeded packages."
pacman -Rns xplayer lightdm-gtk-greeter lightdm-gtk-greeter-settings --noconfirm
echo "[alpana_info]: Changing settings."
sed -i 's/lightdm-gtk-greeter/lightdm-slick-greeter/g' /etc/lightdm/lightdm.conf
pacman -Q grub-silent &> /dev/null
if [[ $? -eq 0 ]]; then
echo "[alpana_info]: Patching GRUB Silent."
echo "[alpana_info]: Backing up your grub config."
mv /etc/default/grub /etc/default/grub_unpatched_before_update
echo "[alpana_info]: Downloading patch."
wget -q --show-progress https://gitlab.com/liya5/alpana/-/raw/main/Divya/grub_patched_after_update
echo "[alpana_info]: Copying patch."
mv grub_patched_after_update /etc/default/grub
echo "[alpana_info]: Regenerate GRUB configuation."
grub-mkconfig -o /boot/grub/grub.cfg
echo "[alpana_info]: Now you can access grub menu by pressing esc key at the time of boot."
fi
if [[ $? -eq 1 ]]; then
echo "[alpana_info]: GRUB Silent Patch does not apply for you."
fi
echo "[alpana_info]: Patching VCONSOLE File."
echo "[alpana_info]: Downloading patch."
wget -q --show-progress https://gitlab.com/liya5/alpana/-/raw/main/Divya/vconsole_patched
echo "[alpana_info]: Copying patch."
mv vconsole_patched /etc/vconsole.conf
echo "[alpana_info]: Regenerate MKINITCPIO."
mkinitcpio -P
echo "[alpana_info]: Now you will get a new font during boot in plymouth."
echo "[alpana_info]: Downloading ZSHRC."
cd /tmp
curl -L --output /tmp/tmp-upd/zshrc_conf https://gitlab.com/liya5/liso/-/raw/main/ISO-Profiles/common/airootfs/etc/skel/.zshrc 
echo "[alpana_info]: Copying ZSHRC."
cp /tmp/tmp-upd/zshrc_conf /etc/skel/.zshrc
echo "[alpana_question]: What is your username?"
read username
mv /home/$username/.zshrc /home/$username/.zshrc_bak.bak
cp -rf /tmp/tmp-upd/zshrc_conf /home/$username/.zshrc
echo "[alpana_info]: Finished New ZSHRC, resource it."