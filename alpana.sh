codename=Divya
check_error() {
if [[ $? -eq 1 ]]; then
echo "[alpana_error]: Unable To Update, Check And Resolve The Errors, Contact System Admin (or) Liya Support ###"
exit 1
fi
if [[ $? -eq 0 ]]; then
echo "[alpana_info]: Sucessfully Completed."
fi
}


echo "[alpana_info]: Preparing Alpana $codename."
echo "[alpana_question]: Do You Want To Fully Update Liya[1] [recommended] Or Just Perform Pacman Update[2]"
read fupd
if [[ $fupd == 1 ]]; then
echo "[alpana_info]: Updaing Native Packages."
sudo pacman -Syu
check_error
echo "[alpana_info]: Updating The AUR Packages."
paru -Sua
check_error
echo "[alpana_info]: Downloading $codename Release Scripts."
wget -q --show-progress https://gitlab.com/liya5/alpana/-/raw/main/$codename/info.txt
wget -q --show-progress https://gitlab.com/liya5/alpana/-/raw/main/$codename/script.sh
wget -q --show-progress https://gitlab.com/liya5/alpana/-/raw/main/$codename/future_message.txt
mkdir -p /tmp/tmp-upd
mv info.txt script.sh future_message.txt /tmp/tmp-upd
sudo chmod +x /tmp/tmp-upd/script.sh
cd /tmp/tmp-upd
cat info.txt
pkexec /tmp/tmp-upd/script.sh
check_error
cat /tmp/tmp-upd/future_message.txt
if [ ! -d "/var/liya/alpana" ]; then
sudo mkdir -p /var/liya/alpana
fi
sudo touch /var/liya/alpana/$codename.update_finished
fi
if [[ $fupd == 2 ]]; then
sudo pacman -Syu
fi
